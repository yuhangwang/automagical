package.path = package.path .. ";?.lua"
local FX = require("FunctionalX")
local L = FX.lists
local config = {
  module = "autoscript"
}
local legal_flags = {
  "-a",
  "-f",
  "-t"
}
local usage = [[Usage: auto target1 
       auto target1 -f my_autoscript 
       auto target1 -f my_autoscript -a arg1 arg2
]]
local is_flag
is_flag = function(str)
  if str == nil then
    return false
  end
  for _index_0 = 1, #legal_flags do
    local x = legal_flags[_index_0]
    if str == x then
      return true
    end
  end
  return false
end
local is_not_flag
is_not_flag = function(str)
  return not (is_flag(str))
end
local parse
parse = function(input, config, targets, args, flag)
  if flag == nil then
    flag = "-t"
  end
  if #input == 0 then
    return config, targets, args
  end
  if is_flag((L.head(input))) then
    return parse((L.tail(input)), config, targets, args, (L.head(input)))
  end
  local _exp_0 = flag
  if "-t" == _exp_0 then
    return parse((L.tail(input)), config, (L.append(targets, (L.head(input)))), args, "-t")
  elseif "-f" == _exp_0 then
    return parse((L.tail(input)), (L.merge(config, {
      module = (L.head(input))
    })), targets, args, "-t")
  elseif "-a" == _exp_0 then
    return parse((L.tail(input)), config, targets, (L.append(args, (L.head(input)))), "-a")
  else
    return parse({ }, config, targets, args)
  end
end
local dashed_line
dashed_line = function(n, symbol, accum)
  if symbol == nil then
    symbol = "-"
  end
  if accum == nil then
    accum = ""
  end
  if n == 0 then
    return accum
  else
    return dashed_line(n - 1, symbol, symbol .. accum)
  end
end
local work
work = function(module_name, targets, args)
  local M = require(module_name)
  for _index_0 = 1, #targets do
    local target = targets[_index_0]
    if M[target] ~= nil then
      M[target](unpack(args))
    end
  end
end
local main
main = function(input, default_config)
  if #input == 0 then
    return usage
  end
  local user_config, user_targets, user_args = parse(input, default_config, { }, { })
  work(user_config.module, user_targets, user_args)
  return dashed_line(80)
end
return print(main(arg, config))
