package.path = package.path..";?.lua"
L = (require "FunctionalX").lists

legal_flags = {
    "-a", -- user arguments
    "-t", -- user target names
}

usage = [[
-------------------------------
 |  | ||| Automagical ||| |  |
-------------------------------
Usage: auto module1
       auto module1 -a arg1
       auto -t module1
       auto -t module1 -a arg1
       auto -t module1 -a arg1 -t module2 -a arg2 ...
]]

-- check whether a string is a flag
is_flag = (str) ->
    return false if str == nil 
    for x in *legal_flags
        if str == x
            return true
    return false

-- remove file extension ".lua"
rm_ext = (str) -> string.match(str, "^[.]?[\\/]?[^.]+")

-- parse command line arguments and return two lists: 
--   1. list of target module names (as string) specified by the "-t" flag
--   2. list of command line arguments specified by "-a" flag
parse = (terminal_args) ->
    aux = (terminal_args, targets, target_args, flag="-t") ->
        return targets, target_args if #terminal_args == 0
        if is_flag (L.head terminal_args)
            new_flag = L.head terminal_args
            return aux (L.tail terminal_args), targets, target_args, new_flag
        else 
            switch flag
                when "-t" then return aux (L.tail terminal_args), (L.append targets, rm_ext(L.head(terminal_args))), target_args, "-t"
                when "-a" then return aux (L.tail terminal_args), targets, (L.append target_args, (L.head terminal_args)), "-t"
                else return aux {}, targets, target_args
    return aux terminal_args, {}, {}

work = (targets, args) ->
    if #targets == 0
        return "DONE!"
    else
        M = require (L.head targets)
        M.main (L.head args)
        return work (L.tail targets), (L.tail args)

main = (terminal_args) ->
    return usage if #terminal_args == 0
    target_modules, target_args = parse terminal_args
    return usage if #target_modules == 0
    work target_modules, target_args
-----------------------------------------
print main arg